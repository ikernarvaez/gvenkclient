# ----------------------------------------------------------------------
# Class: BeaconsController. 
# Controlador de las vistas del CRUD de Beacons.
# Consume el servicio Rest de beacons del Web API de iBeacons.
#-----------------------------------------------------------------------
class BeaconsController < ApplicationController
  # Antes de las acciones de :show y :edit se llama al método set_beacon
  before_action :set_beacon, only: [:show, :edit]

  # Requiere protection de forgery en las peticiones de crear y actualizar
  protect_from_forgery only: [:create, :update]
  
  # Url del web service de Beacons
  @@WEBSERVICE_URL = "http://localhost"

  # Puerto del web service de beacons
  @@WEBSERVICE_PORT = 3030

  require "net/http"
  require "uri"

  # GET /beacons
  # GET /beacons.json
  # Muestra la lista de beacons
  def index
    layout = "application"
    getAllBeacons()
  end

  # GET /beacons/1
  # GET /beacons/1.json
  # Muestra la información de un beacon
  # [param] id del beacon
  def show
     layout = "application"
  end

  # GET /beacons/new
  # Muestra la vista para crear un nuevo beacon
  def new
    @beacon = Beacon.new
  end

  # GET /beacons/1/edit
  # Muestra la vista para editar un beacon
  def edit
  end

  # POST /beacons
  # POST /beacons.json
  # Crea un beacon nuevo
  def create

    begin

      # Crea una instancia de beacon a partir de los parametros del POST
      @beacon = Beacon.new(beacon_params)

      if (@beacon.invalid?)
        render :action => "new/"
        return
      end

      departament = @beacon.departamento
      neg = @beacon.negocio
      rango = @beacon.rango_deteccion

      # Crea un mensaje json con la información del beacon
      jsonBody ={ departamento: departament, negocio: neg, rango_deteccion: rango}.to_json
      
      # Consume el Rest service de beacons
      uri = URI.parse(@@WEBSERVICE_URL + "/beacons")
      http = Net::HTTP.new(uri.host, @@WEBSERVICE_PORT)
      request = Net::HTTP::Post.new(uri.request_uri)
      request.body = jsonBody
      request.content_type = "application/json"
      request.add_field("Authorization", "Token token=" + APIkey)
      response = http.request(request)

      # Si la respuesta del servicio fue exitosa
      case response
      when Net::HTTPSuccess
          getAllBeacons()
          @success = true
          @mensaje = "El beacon fue agregado exitsamente."
          render "index"

      else
          "<b>Detalle del error:</b>" + response.body + " </br> <b>Codigo HTTP:</b> " + response.code    
          render "error/index"
      end

    rescue Exception => ex
        @error = "<b>Detalle del error:</b>" + ex.message
        render "error/index"
        return
    end
  end

  # PATCH/PUT /beacons/1
  # PATCH/PUT /beacons/1.json
  # Modifica un beacon
  # [param] id del beacon
  def update
    begin

      # Obtiene el id del beacon
      beaconId = params[:id];

      # Crea una nueva instancia a partir de los parametros del PATCH/PUT
      @bc = Beacon.new(beacon_params)

      if (!@bc.valid?)
        @beacon = @bc
        @beacon.id = beaconId;
        render :action => "edit", :id => beaconId
        return
      end

      departament = @bc.departamento
      neg = @bc.negocio
      rango = @bc.rango_deteccion

      # Crea el mensaje json con la información del beacon
      jsonBody ={ beacons:{ id: beaconId, departamento: departament, negocio: neg, rango_deteccion:       rango}}.to_json

      # Consume el servicio Rest de beacons
      uri = URI.parse(@@WEBSERVICE_URL)
      http = Net::HTTP.new(uri.host, @@WEBSERVICE_PORT)
      request = Net::HTTP::Patch.new("/beacons/"+ beaconId)
      request.body = jsonBody
      request.content_type = "application/json"
      request.add_field("Authorization", "Token token=" + APIkey)
      response = http.request(request)

      # Si la respuesta fue exitosa
      case response
      when Net::HTTPSuccess
          getAllBeacons()
          @success = true
          @mensaje = "El beacon fue modificado exitosamente."
          render "index"
      else    
          @error = "<b>Detalle del error:</b>" + response.body + " </br> <b>Codigo HTTP:</b> " + response.code    
          render "error/index"
      end

    rescue Exception => ex
        @error = "<b>Detalle del error:</b>" + ex.message
        render "error/index"
        return
    end
  end

  # DELETE /beacons/1
  # DELETE /beacons/1.json
  # Elimina un beacon
  # [param] id del beacon
  def destroy
    begin
      # obtiene el id del beacon
      beaconId = params[:id];

      # consume el servicio Rest de beacons
      uri = URI.parse(@@WEBSERVICE_URL)
      http = Net::HTTP.new(uri.host, @@WEBSERVICE_PORT)
      request = Net::HTTP::Delete.new("/beacons/"+ beaconId)
      request.content_type = "application/json"
      request.add_field("Authorization", "Token token=" + APIkey)
      response = http.request(request)

      # Si la respuesta fue exitosa
      case response
      when Net::HTTPSuccess
          getAllBeacons()
          @success = true
          @mensaje = "El beacon fue eliminado exitosamente."
          render "index"
      else    
          @error = "<b>Detalle del error:</b>" + response.body + " </br> <b>Codigo HTTP:</b> " + response.code    
          render "error/index"
      end
    rescue Exception => ex
        @error = "<b>Detalle del error:</b>" + ex.message
        render "error/index"
        return
    end
  end

  private
    # Callback de uso común para crear una instancia de beacon a partir del id del beacon
    def set_beacon
      begin

        beaconId = params[:id];
      
        url = URI.parse(@@WEBSERVICE_URL+ "/beacons/" + String(beaconId))
        req = Net::HTTP::Get.new(url)
        req.add_field("Authorization", "Token token=" + APIkey)


        res = Net::HTTP.new(url.host, @@WEBSERVICE_PORT).start do |http|
            # realiza una petición get
            http.request(req)
        end
        response = JSON.parse res.body
  #      render :plain => response

        # Crea la instancia del beacon a partir de la respuesta del servicio
        @beacon = Beacon.new
        @beacon.id = Integer(params[:id])
        @beacon.departamento = response["departamento"]
        @beacon.negocio = response["negocio"]
        @beacon.rango_deteccion = response["rango_deteccion"]

      rescue Exception => ex
        @error = ex.message
        render "error/index"
        return
      end
    end
    
    # Obtiene los parámetros del beacon de forma segura.
    # Never trust parameters from the scary internet, only allow the white list through.
    def beacon_params
      params.require(:beacon).permit(:departamento, :negocio, :rango_deteccion)
    end

    # Obtiene la lista de todos los beacons    
    def getAllBeacons
      begin
        # Consume el Rest service de beacons
        url = URI.parse(@@WEBSERVICE_URL+ "/beacons")
        req = Net::HTTP::Get.new(url)
        req.add_field("Authorization", "Token token=" + APIkey)


        res = Net::HTTP.new(url.host, @@WEBSERVICE_PORT).start do |http|
            # realiza una petición get
            http.request(req)
        end

        # Interpreta la respuesta como Json
        @results = JSON.parse res.body

        # Lista de beacons
        @beacons = []

        # Para cada elemento en el json
        @results.each do |b|

          # Crea una instancia de Beacon
          beacon = Beacon.new
          beacon.id = b["id"]
          beacon.departamento = b["departamento"]
          beacon.negocio = b["negocio"]
          beacon.rango_deteccion = b["rango_deteccion"]

          # Agrega el beacon a la lista
          @beacons << beacon
        end 
      rescue Exception => ex
          @error = "<b>Detalle del error:</b>" + ex.message
          render "error/index"
          return
      end
    end
end
