class Beacon < ActiveRecord::Base
   self.table_name = "beacon"

   validates :departamento, 
    :presence => {
   		message: "es un campo obligatorio."
   	}, 
   	length: {
   			maximum: 30, 
   			message:"no puede contar con más de 30 caractéres."
   		}

   	validates :negocio, 
    :presence => {
   		message: "es un campo obligatorio."
   	}, 
   	length: {
   			maximum: 30, 
   			message:"no puede contar con más de 30 caractéres."
   		}

   	validates :rango_deteccion, 
    :presence => {
   		message: "es un campo obligatorio."
   	}, 
   	length: {
   			maximum: 30, 
   			message:"no puede contar con más de 30 caractéres."
   		}
   #validates: departamento, length: { maximum: 30}, message:"Longitud máxima excedida." 
end
