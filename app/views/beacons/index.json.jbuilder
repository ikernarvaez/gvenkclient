json.array!(@beacons) do |beacon|
  json.extract! beacon, :id, :string, :string, :string
  json.url beacon_url(beacon, format: :json)
end
