class CreateBeacons < ActiveRecord::Migration
  def change
    create_table :beacon do |t|
      t.string :departamento
      t.string :negocio
      t.string :rango_deteccion

      t.timestamps
    end
  end
end
