# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141108215753) do

  create_table "beacon", force: true do |t|
    t.string   "departamento"
    t.string   "negocio"
    t.string   "rango_deteccion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "haciendas", force: true do |t|
    t.string   "rfc"
    t.string   "calle"
    t.string   "numero"
    t.string   "colonia"
    t.string   "ciudad"
    t.string   "codigo_postal"
    t.string   "estado"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "negocios", force: true do |t|
    t.string   "nombre"
    t.string   "giro"
    t.string   "calle"
    t.string   "numero"
    t.string   "colonia"
    t.string   "ciudad"
    t.string   "codigo_postal"
    t.string   "estado"
    t.integer  "productoId"
    t.integer  "haciendaId"
    t.string   "producto"
    t.string   "hacienda"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "productos", force: true do |t|
    t.string   "costo"
    t.string   "image_url"
    t.string   "categoria"
    t.integer  "promocionId"
    t.integer  "beaconId"
    t.string   "promocion"
    t.string   "beacon"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "promocions", force: true do |t|
    t.string   "terminos_condiciones"
    t.string   "image_url"
    t.string   "leyenda"
    t.string   "codigo"
    t.string   "fecha_inicio"
    t.string   "fecha_fin"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nombre"
    t.string   "password"
    t.integer  "establecimientoId"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "establecimiento"
  end

end
